# [1.1.0](https://gitlab.com/beepbeepgo/public/libraries/npm/semantic-release-python/compare/1.0.3...1.1.0) (2022-12-16)


### Features

* update packages ([e7248f2](https://gitlab.com/beepbeepgo/public/libraries/npm/semantic-release-python/commit/e7248f292b7208ba38fe0e3642dc85836e6897e6))

## [1.0.3-issue-update-packages.1](https://gitlab.com/beepbeepgo/public/libraries/npm/semantic-release-python/compare/1.0.2...1.0.3-issue-update-packages.1) (2022-12-16)


### Bug Fixes

* clean lock file ([68aa281](https://gitlab.com/beepbeepgo/public/libraries/npm/semantic-release-python/commit/68aa28199fc102cdc1d58029d617ee90f4c6f5fe))
* convert release config ([2302d54](https://gitlab.com/beepbeepgo/public/libraries/npm/semantic-release-python/commit/2302d544f4ce78dccb93f37835bd9676fbe4fd4c))
* release config was invalid ([087d015](https://gitlab.com/beepbeepgo/public/libraries/npm/semantic-release-python/commit/087d0154d08b41f2dca6a473ad23a87a8984c4e8))
